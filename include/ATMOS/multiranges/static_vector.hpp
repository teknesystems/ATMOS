// A static collection of an integral type T, with the operations of a stack
// supported at compile time, along with some compile-time algorithms based off
// this.

#ifndef ATMOS_MULTIARRAY_STATIC_VECTOR_INCLUDED
#define ATMOS_MULTIARRAY_STATIC_VECTOR_INCLUDED

#include "multiarray_types.hpp"

#include <iterator>
#ifdef DOCTEST_LIBRARY_INCLUDED // Necessary for tests
#include <algorithm>
#endif

namespace ATMOS
{

  // Static vector types

  template<class T, T...>
  struct static_vector
  {
    // Associated types
    typedef T element_type;
    typedef dim_t size_type;

    // Information
    inline static constexpr bool is_empty() {return true;}
    inline static constexpr size_type size() {return 0;}

    // Templates
    using empty_t = static_vector<T>;
    template<T N> using push_t = static_vector<T, N>;
    template<template<T...> class O> using apply_t = O<>;

    // Access
    inline static constexpr T at(sdim_t idx) {return 0;}

    // Objects
    template<T N>
    inline static constexpr push_t<N> push() {return {};}
  };

  template<class T, T N, T... Ns>
  struct static_vector<T, N, Ns...>
  {
    // Associated types
    typedef T element_type;
    typedef dim_t size_type;
    typedef const element_type* iterator;
    typedef const element_type* const_iterator;

    // Information
    inline static constexpr bool is_empty() {return true;}
    inline static constexpr size_type size() {return 1 + sizeof...(Ns);}
    inline static constexpr T top() {return N;}

    // Data
    static constexpr const element_type
    elements[1 + sizeof...(Ns)] = {N, Ns...};
    inline static constexpr const element_type* begin() {return elements;}
    inline static constexpr const element_type* end() {return begin() + size();}

    // Templates
    using empty_t = static_vector<T>;
    template<T K> using push_t = static_vector<T, K, N, Ns...>;
    using pop_t = static_vector<T, Ns...>;
    template<template<T...> class O> using apply_t = O<N, Ns...>;

    // Objects
    template<T K>
    inline static constexpr push_t<K> push() {return {};}
    inline static constexpr pop_t pop() {return {};}

    // Operations
    template<class T2, class... Ts>
    inline static constexpr decltype(N * T2()) dot(T2 K, Ts... Ks)
    noexcept {
      static_assert(sizeof...(Ks) <= sizeof...(Ns), "Input vector too long!");
      return K * N + pop_t::dot(Ks...);
    }
    template<class T2>
    inline static constexpr decltype(T() * T2()) dot(T2 K) noexcept
    {return K * N;}
    template<class T2, T2... Ks>
    inline static constexpr decltype(T() * T2()) dot(static_vector<T2, Ks...>)
    noexcept {return dot(Ks...);}

    // Elementwise access
    inline static constexpr T at(dim_t idx) noexcept
    {
      return idx?
      pop_t::at(idx - 1):top();
    }

    inline constexpr T operator[](dim_t idx) const noexcept {return at(idx);}

    // Partial dot product on a range
    template<class IT>
    inline static decltype(T() * *IT()) dot(IT begin, IT end)
    {
      dim_t count = 0;
      decltype(T() * *begin) result = 0;
      while(begin != end)
      {
        result += *begin * at(count);
        ++count;
        ++begin;
      }
      return result;
    }

    // Partial dot product on a container
    template<class CT>
    inline static auto dot(const CT& cont) -> decltype(T() * *std::begin(cont))
    {
      return dot(std::begin(cont), std::end(cont));
    }

  };

  template<class T, T N, T... Ns>
  constexpr const T static_vector<T, N, Ns...>::elements[1 + sizeof...(Ns)];

  // Predefined static vectors
  template<class T, dim_t k>
  struct make_static_zero_vector
  {
    typedef typename make_static_zero_vector<T, k-1>::type::
    template push_t<0> type;
  };

  template<class T>
  struct make_static_zero_vector<T, 0>
  {
    typedef static_vector<T> type;
  };

  template<class T, dim_t k>
  using static_zero_vector = typename make_static_zero_vector<T, k>::type;

  // Static vector algorithms

  template<dim_t k, class P>
  struct slice_static_vector
  {
    typedef typename slice_static_vector<k - 1, typename P::pop_t>::back back;
    typedef typename slice_static_vector<k - 1, typename P::pop_t>
    ::front::template push_t<P::top()> front;
  };

  template<class P>
  struct slice_static_vector<0, P>
  {
    typedef P back;
    typedef typename P::empty_t front;
  };

  template<dim_t k, class P>
  using static_vector_back = typename slice_static_vector<k, P>::back;
  template<dim_t k, class P>
  using static_vector_front = typename slice_static_vector<k, P>::front;

  template<dim_t k, class P>
  inline constexpr static_vector_back<k, P> get_static_vector_back(P)
  {return {};}
  template<dim_t k, class P>
  inline constexpr static_vector_front<k, P> get_static_vector_front(P)
  {return {};}

  // Static-static vector operations
  template<class T, class T2, T... Ns, T2... Ks>
  inline constexpr static_vector<decltype(T() + T2()), (Ns + Ks)...>
  operator+(static_vector<T, Ns...>, static_vector<T2, Ks...>) {return {};}

  template<class T, class T2, T... Ns, T2... Ks>
  inline constexpr static_vector<decltype(T() - T2()), (Ns - Ks)...>
  operator-(static_vector<T, Ns...>, static_vector<T2, Ks...>) {return {};}

  template<class T, class T2, T... Ns, T2... Ks>
  inline constexpr static_vector<decltype(T() % T2()), (Ns % Ks)...>
  operator%(static_vector<T, Ns...>, static_vector<T2, Ks...>) {return {};}

  // Equality
  template<class T1, class T2, T1 N, T2 K, T1... Ns, T2... Ks>
  inline constexpr bool operator==
  (static_vector<T1, N, Ns...> u, static_vector<T2, K, Ks...> v)
  {
    return (sizeof...(Ns) == sizeof...(Ks)) &&
    (N == K) && (u.pop() == v.pop());
  }
  template<class T1, class T2>
  inline constexpr bool operator==
  (static_vector<T1>, static_vector<T2>) {return true;}
  template<class T1, class T2, T2 N, T2... Ns>
  inline constexpr bool operator==
  (static_vector<T1>, static_vector<T2, N, Ns...>) {return false;}
  template<class T1, class T2, T1 N, T1... Ns>
  inline constexpr bool operator==
  (static_vector<T1, N, Ns...>, static_vector<T2>) {return false;}
  template<class T1, class T2, T1... Ns, T2... Ks>
  inline constexpr bool operator!=
  (static_vector<T1, Ns...> u, static_vector<T2, Ks...> v)
  {
    return !(u == v);
  }

  // Ordering
  template<class T1, class T2, T1... Ns, T2... Ks>
  inline constexpr bool operator<
  (static_vector<T1, Ns...> u, static_vector<T2, Ks...> v)
  {
    static_assert(sizeof...(Ns) == sizeof...(Ks),
    "Cannot order static_vectors of different sizes");
    return (u.top() < v.top()) ||
    ((u.top() == v.top()) && (u.pop() < v.pop()));
  }
  template<class T1, class T2, T1 N, T2 K>
  inline constexpr bool operator<
  (static_vector<T1, N>, static_vector<T2, K>)
  {
    return N < K;
  }
  template<class T1, class T2, T1... Ns, T2... Ks>
  inline constexpr bool operator<=
  (static_vector<T1, Ns...> u, static_vector<T2, Ks...> v)
  {
    static_assert(sizeof...(Ns) == sizeof...(Ks),
    "Cannot order static_vectors of different sizes");
    return (u.top() < v.top()) ||
    ((u.top() == v.top()) && (u.pop() <= v.pop()));
  }
  template<class T1, class T2, T1 N, T2 K>
  inline constexpr bool operator<=
  (static_vector<T1, N>, static_vector<T2, K>)
  {
    return N <= K;
  }
  template<class T1, class T2, T1... Ns, T2... Ks>
  inline constexpr bool operator>
  (static_vector<T1, Ns...> u, static_vector<T2, Ks...> v)
  {
    return v < u;
  }
  template<class T1, class T2, T1... Ns, T2... Ks>
  inline constexpr bool operator>=
  (static_vector<T1, Ns...> u, static_vector<T2, Ks...> v)
  {
    return v <= u;
  }

  // Guaranteed positive modulo for usage with wrapped arrays
  template<class T, class T2, T... Ns, T2... Ks>
  inline constexpr static_vector<decltype(T() % T2()), (((Ns % Ks) + Ks)%Ks)...>
  wrap_indices(static_vector<T, Ns...>, static_vector<T2, Ks...>) {return {};}

  template<class T, class T2, T... Ns, T2... Ks>
  inline constexpr decltype(T() * T2())
  operator*(static_vector<T, Ns...> v, static_vector<T2, Ks...> u)
  {return v.dot(u);}

  // Constexpr functions
  inline constexpr dim_t product() {return 1;}

  template<class T>
  inline constexpr T product(T A) {return A;}

  //TODO: fix return type
  template<class T, class... Ts>
  inline constexpr T product(T N, Ts... Ns) {return N * product(Ns...);}

  template<class T, T... Ns>
  inline constexpr T product(static_vector<T, Ns...>) {return product(Ns...);}

  // Doctests
  #ifdef DOCTEST_LIBRARY_INCLUDED

  TEST_CASE("Static vector dot products, additions and slices are correct")
  {
    dim_t v_arr[5] = {1, 3, 5, 7, 9};
    dim_t plus_arr[5] = {2, 6, 10, 14, 18};
    dim_t zero_arr[5] = {0, 0, 0, 0, 0};
    static_vector<dim_t, 1, 3, 5, 7, 9> v;
    static_vector<dim_t, 1, 1, 1, 1, 1> s;
    static_zero_vector<dim_t, 5> z;

    // Basic properties
    CHECK_EQ(v.size(), 5);
    CHECK_EQ(v.top(), 1);

    // Indices and internal array
    for(dim_t i = 0; i < v.size(); i++)
    {
      CHECK_EQ(v[i], v.begin()[i]);
    }
    CHECK(std::equal(v.begin(), v.end(), v_arr));

    // Sums and differences
    CHECK(std::equal((v + v).begin(), (v + v).end(), plus_arr));
    CHECK(std::equal((v - v).begin(), (v - v).end(), zero_arr));

    // Pushing and popping
    CHECK_EQ(v.push<1000>().top(), 1000);
    CHECK(std::equal(v.push<1000>().begin() + 1, v.push<1000>().end(), v_arr));
    CHECK(std::equal(v.pop().begin(), v.pop().end(),v_arr + 1));

    // Slices
    auto v_f = get_static_vector_front<2>(v);
    auto v_b = get_static_vector_back<2>(v);
    CHECK_EQ(v_f.size(), 2);
    CHECK_EQ(v_b.size(), 3);
    CHECK(std::equal(v_f.begin(), v_f.end(), v.begin()));
    CHECK(std::equal(v_b.begin(), v_b.end(), v.begin() + 2));

    // Static dot products
    CHECK_EQ(s * v, 25);
    CHECK_EQ(v * s.pop(), 16);

    // Dynamic dot products
    int dyn[4] = {1, 1, 1, 1};
    CHECK_EQ(v.dot(dyn, dyn + 3), 9);
    CHECK_EQ(v.dot(dyn), 16);

    // Modulus and index wrapping
    static_vector<sdim_t, -9, -5> negative;
    static_vector<dim_t, 4, 4> w;
    CHECK_EQ((negative % w).top(), negative.top() % w.top());
    CHECK_EQ(wrap_indices(negative, w).top(), 3);
    CHECK(std::equal((v % s).begin(), (v % s).end(), zero_arr));

    // Zero vectors
    CHECK_EQ(z.size(), 5);
    CHECK(std::equal(z.begin(), z.end(), zero_arr));

    // Comparison
    CHECK_EQ(v, v);
    CHECK_NE(s, v);
    CHECK_NE(v, v_f);
    CHECK_LT(s, v);
    CHECK_GT(v, s);
    CHECK_EQ(static_vector<dim_t>(), static_vector<dim_t>());

    // Products
    CHECK_EQ(product(1, 2, 3), 6);
  }

  #endif

}

#endif
