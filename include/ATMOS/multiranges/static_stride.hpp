// A static stride-vector satisfying the Stride concept.
// Can be used for multidimensional iterators and to compose size-stride storage

// Header guard. Skips over entire header if already included.
#ifndef ATMOS_MULTIARRAY_STATIC_STRIDE_INCLUDED
#define ATMOS_MULTIARRAY_STATIC_STRIDE_INCLUDED

#include "multiarray_types.hpp"
#include "static_vector.hpp"
#ifdef DOCTEST_LIBRARY_INCLUDED // Neccessary for tests
#include <algorithm>
#endif

namespace ATMOS
{

  namespace storage
  {

    // Treat a static vector as a stride vector
    template<class P>
    struct static_stride_vector: public P
    {
      // Associated types
      typedef typename P::size_type size_type;
      typedef typename P::element_type difference_type;
      typedef typename P::element_type index;
      template<dim_t k>
      struct substride
      {
        typedef static_stride_vector<static_vector_back<k, P>> type;
        inline static constexpr bool is_static() {return true;}
      };

      // Constants
      static constexpr size_type dimensionality = P::size();
      static_assert(dimensionality > 0, "Dimensionality cannot be zero!");

      // Data member functions
      inline static constexpr const index* strides()
      {return P::begin();}
      inline static const index* index_bases()
      {return static_zero_vector<index  , dimensionality>::begin();}

      inline static constexpr index origin() {return 0;}
      inline static constexpr size_type num_dimensions()
      {return dimensionality;}

      // Single-index access member functions
      inline static constexpr index stride() {return P::top();}
      inline static constexpr index stride(dim_t k) {return P::at(k);}
      inline static constexpr index access(index idx) {return stride() * idx;}
      inline constexpr index operator[](index idx) const{return access(idx);}

      // Container access member function
      template<class T>
      inline static constexpr index access(const T& container)
      {return P::dot(container);}
      template<class T>
      inline constexpr index operator()(const T& container) const
      {return access(container);}

      template<dim_t k>
      inline static constexpr typename substride<k>::type substride_obj()
      {return {};}

      // Information member functions
      inline static constexpr bool static_strides() {return true;}
      inline static constexpr bool static_origin() {return true;}
      inline static constexpr bool static_bases() {return true;}
      inline static constexpr bool trivial_origin() {return true;}
      inline static constexpr bool trivial_bases() {return true;}
      inline static constexpr bool dynamic_strides() {return false;}
      inline static constexpr bool dynamic_offset() {return false;}

    };

    template<class P>
    constexpr typename P::size_type static_stride_vector<P>::dimensionality;

    // Operations for static stride vectors
    template<class P1, class P2>
    inline constexpr static_stride_vector<decltype(P1() + P2())>
    operator+(static_stride_vector<P1>, static_stride_vector<P2>)
    {return {};}
    template<class P1, class P2>
    inline constexpr static_stride_vector<decltype(P1() - P2())>
    operator-(static_stride_vector<P1>, static_stride_vector<P2>)
    {return {};}

    // Generate a static stride vector from a parameter pack of strides
    template<sdim_t... Ns>
    using static_stride =
    static_stride_vector<static_vector<sdim_t, Ns...>>;

    #ifdef DOCTEST_LIBRARY_INCLUDED

    // Testing if static strides work as expected.
    TEST_CASE("Static stride functionality")
    {
      static_stride<16, 8, 4, 2, 1> S;
      static_stride<10, 5, 1, 0, 0> T;
      sdim_t plus_arr[5] = {26, 13, 5, 2, 1};
      sdim_t minus_arr[5] = {6, 3, 3, 2, 1};

      // Basic functionality
      CHECK_EQ(S.dimensionality, 5);
      CHECK_EQ(S.strides()[0], S.stride());
      CHECK_EQ(S.stride(), 16);
      CHECK_EQ(S.strides()[4], 1);
      CHECK_EQ(S.index_bases()[0], 0);
      CHECK_EQ(S.access(2), 32);
      CHECK_EQ(S.access(-1), -16);
      CHECK_EQ(S[4], 64);

      // Substorage functionality
      auto s = S.substride_obj<2>();
      CHECK_EQ(s.dimensionality, 3);
      CHECK_EQ(s.strides()[0], s.stride());
      CHECK_EQ(s.stride(), 4);
      CHECK_EQ(s.access(2), 8);
      CHECK(std::equal(s.begin(), s.end(), S.begin() + 2));

      // Operations
      CHECK(std::equal((S + T).begin(), (S + T).end(), plus_arr));
      CHECK(std::equal((S - T).begin(), (S - T).end(), minus_arr));

    }

    #endif
  }

}

#endif
