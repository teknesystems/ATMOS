// A view of a multiarray which does *not* own storage.

#ifndef ATMOS_MULTIARRAY_VIEW_INCLUDED
#define ATMOS_MULTIARRAY_VIEW_INCLUDED

#include <utility>
#include <iterator>

namespace ATMOS
{

  template<class iterator_type, class storage_order>
  struct multiarray_view: public storage_order
  {
    // Subarray types
    template<dim_t k>
    struct array_view
    {
      typedef multiarray_view<
        iterator_type,
        typename storage_order::template substorage<k>::type
      > type;
    };

    // Associated types
    typedef typename storage_order::size_type size_type;
    typedef typename storage_order::difference_type difference_type;
    typedef typename storage_order::index index;
    typedef typename std::iterator_traits<iterator_type>::value_type value_type;

    // Data
    iterator_type data_iter;

  };

}

#endif
