// Various static storage configurations, all satisfying the Storage concept,
// including:
// - Arbitrary size/stride pairs
// - Row major (C-style) / Column major (Fortran-style) storage
// - Permutations of contiguous storage (tagged as contiguous storage)
// - Null stride (e.g. all one value)
#ifndef ATMOS_MULTIARRAY_STATIC_STORAGE_INCLUDED
#define ATMOS_MULTIARRAY_STATIC_STORAGE_INCLUDED

#include "multiarray_types.hpp"
#include "static_vector.hpp"
#include "utility/cpp11_constexpr_forward.hpp"

#include <type_traits>

#ifdef DOCTEST_LIBRARY_INCLUDED // Necessary for tests
#include "static_stride.hpp"
#include <algorithm>
#endif

namespace ATMOS
{
  namespace storage
  {

    template<class P, class stride>
    struct static_storage: public stride
    {
      // Check parameter pack and stride dimensionality are compatible
      static_assert(P::size() == stride::dimensionality,
    "Stride and size dimensions are incompatible!");

      // Associated types
      typedef typename stride::size_type size_type;
      typedef typename stride::difference_type difference_type;
      typedef typename stride::index index;

      // Templates
      template<dim_t k>
      using substride = typename stride:: template substride<k>;
      template<dim_t k>
      struct substorage
      {
        typedef static_storage<
          static_vector_back<k, P>,
          typename substride<k>::type
        > type;
        inline static constexpr bool is_static()
        {return substride<k>::is_static();}
      };

      // Data member functions
      inline static constexpr const size_type* shape()
      {return P::begin();}
      inline static constexpr size_type num_elements()
      {return product(P());}
      inline static constexpr size_type capacity() {return num_elements();}

      // Access member functions
      inline static constexpr size_type size() {return P::top();}

      // Substorage
      // Enable if static substride exists

      template<dim_t k>
      inline static constexpr typename std::enable_if<
      substorage<k>::is_static(),
      typename substorage<k>::type>::type substorage_obj()
      {return stride:: template substride_obj<k>();}
      // Enable otherwise
      template<dim_t k>
      inline constexpr typename std::enable_if<
      !(substorage<k>::is_static()),
      typename substorage<k>::type>::type substorage_obj() const
      {return stride:: template substride_obj<k>();}

      // Information
      inline static constexpr bool dynamic_size() {return false;}

      // Constructors (delegate to stride)
      template<class... Ts>
      constexpr static_storage(Ts&&... args):
      stride(ATMOS_CPP11_CONSTEXPR_FORWARD<Ts>(args)...) {}
    };

    template<class stride, dim_t... Ns>
    using static_bounds = static_storage<static_vector<dim_t, Ns...>, stride>;

    // Doctests
    #ifdef DOCTEST_LIBRARY_INCLUDED

    TEST_CASE("Static storage functionality")
    {
      static_bounds<static_stride<1, 2, 3>, 2, 2, 2> S;
      dim_t stride_arr[3] = {1, 2, 3};
      dim_t size_arr[3] = {2, 2, 2};

      // Basic functionality
      CHECK_EQ(S.num_dimensions(), 3);
      CHECK_EQ(S[4], 4);
      CHECK_EQ(S[-4], -4);

      // Data
      CHECK(std::equal(S.shape(), S.shape() + S.num_dimensions(), size_arr));
      CHECK(std::equal(S.strides(), S.strides() + S.num_dimensions(),
      stride_arr));

      // Substorage
      auto s = S.substorage_obj<1>();
      CHECK(std::equal(s.shape(), s.shape() + s.num_dimensions(),
      size_arr + 1));
      CHECK(std::equal(s.strides(), s.strides() + s.num_dimensions(),
      stride_arr + 1));
      CHECK_EQ(s[4], 8);
      CHECK_EQ(s[-4], -8);
    }

    TEST_CASE("Static storage can be implicitly converted to stride")
    {
      static_bounds<static_stride<1, 2, 3>, 2, 2, 2> S;
      static_stride<1, 2, 3> s = S;
      CHECK_EQ(s.dimensionality, S.dimensionality);
      CHECK_EQ(s.stride(), S.stride());
      CHECK(std::equal(s.begin(), s.end(), S.begin()));
    }

    #endif

  }
}

#endif
