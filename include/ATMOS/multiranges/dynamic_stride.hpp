// A dynamic stride-vector satisfying the Stride concept.

// Header guard. Skips over entire header if already included
#ifndef ATMOS_MULTIARRAY_DYNAMIC_STRIDE_INCLUDED
#define ATMOS_MULTIARRAY_DYNAMIC_STRIDE_INCLUDED

#include "multiarray_types.hpp"
#include "static_vector.hpp"

#include <algorithm> // For std::copy
#include <iterator> // For std::distance

namespace ATMOS
{

  namespace storage
  {

    template<dim_t N>
    struct dynamic_stride
    {
      static_assert(N > 0, "Dimensionality cannot be zero!");

      // Associated types
      typedef dim_t size_type;
      typedef sdim_t difference_type;
      typedef sdim_t index;
      typedef const index* iterator;
      typedef const index* const_iterator;
      template<dim_t k>
      struct substride
      {
        static_assert(k < N, "Dimensionality too low!");
        typedef dynamic_stride<N - k> type;
      };

      // Constants
      static constexpr size_type dimensionality = N;

      // Data
      index stride_vector[dimensionality];

      // Data member functions
      inline constexpr const index* strides() const noexcept {return stride_vector;}
      inline static constexpr const index* index_bases() noexcept
      {return static_zero_vector<sdim_t, dimensionality>::begin();}

      inline static constexpr index origin() {return 0;}
      inline static constexpr size_type num_dimensions()
      {return dimensionality;}

      // Range member functions
      inline static constexpr size_type size() noexcept {return num_dimensions();}
      inline index* begin() noexcept {return stride_vector;}
      inline constexpr const index* begin() const noexcept {return stride_vector;}
      inline index* end() noexcept {return stride_vector + size();}
      inline constexpr const index* end() const noexcept
      {return stride_vector + size();}
      inline constexpr const index* cbegin() const noexcept {return begin();}
      inline constexpr const index* cend() const noexcept {return end();}

      // Access member functions
      inline constexpr index stride() const noexcept {return stride_vector[0];}
      inline constexpr index access(index idx) const noexcept
      {return stride() * idx;}
      inline constexpr index operator[](index idx) const noexcept
      {return access(idx);}

      // Modification member functions
      template<class IT>
      inline void restride(IT begin, IT end)
      {
        if(std::distance(begin, end) > dimensionality)
        {
          end = begin + dimensionality;
        }
        std::copy(begin, end, stride_vector);
      }

      // Substorage access
      template<dim_t k>
      inline dynamic_stride<N - k> substride_obj() const noexcept
      {
        dynamic_stride<N - k> obj;
        std::copy(stride_vector + k, stride_vector + dimensionality,
        obj.stride_vector);
        return obj;
      }

      // Information member functions
      inline static constexpr bool static_strides() {return true;}
      inline static constexpr bool static_origin() {return true;}
      inline static constexpr bool static_bases() {return true;}
      inline static constexpr bool trivial_origin() {return true;}
      inline static constexpr bool trivial_bases() {return true;}
      inline static constexpr bool dynamic_strides() {return false;}
      inline static constexpr bool dynamic_offset() {return false;}

      // Constructors (mainly to remove compiler warning)
      constexpr dynamic_stride() noexcept {}
      template<class... Ts>
      constexpr dynamic_stride(Ts... args) noexcept: stride_vector{args...} {};

    };

    template<dim_t N>
    constexpr dim_t dynamic_stride<N>::dimensionality;

    // Doctests for dynamic vector functionality
    #ifdef DOCTEST_LIBRARY_INCLUDED

    TEST_CASE("Dynamic stride functionality")
    {
      dynamic_stride<3> S {1, 2, 3};

      // Basic functionality
      CHECK_EQ(S.dimensionality, 3);
      CHECK_EQ(S.stride(), 1);
      CHECK_EQ(S.stride(), S.strides()[0]);
      CHECK_EQ(S.access(2), 2);
      CHECK_EQ(S[-2], -2);

      // Substorage functionality
      auto s = S.substride_obj<2>();
      CHECK_EQ(s.dimensionality, 1);
      CHECK_EQ(s.stride(), 3);
      CHECK_EQ(s.access(2), 6);
      CHECK_EQ(s.stride(), S.strides()[2]);

      // Copying storage
      dynamic_stride<3> T = S;
      CHECK_EQ(S.stride(), T.stride());
      CHECK(std::equal(S.begin(), S.end(), T.begin()));

      // Restriding
      T.restride(s.strides(), s.strides() + 2);
      CHECK_EQ(T.stride(), s.stride());

      // Checking copied storage is distinct
      CHECK_NE(T.stride(), S.stride());
    }

    #endif

  }

}

#endif
