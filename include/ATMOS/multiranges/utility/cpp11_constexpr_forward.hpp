// A constexpr implementation of std::forward that works with C++11.
// Defaults to std::forward if C++14 or above is detected
#ifndef ATMOS_CPP11_CONSTEXPR_FORWARD

#if __cplusplus >= 201402L
#include <utility>
#define ATMOS_CPP11_CONSTEXPR_FORWARD std::forward
#else
#include <type_traits>
namespace ATMOS
{
  template<class T>
  constexpr T&& constexpr_forward (typename std::remove_reference<T>::type& t)
  noexcept
  {
    return static_cast<T&&>(t);
  }
}

#define ATMOS_CPP11_CONSTEXPR_FORWARD constexpr_forward
#endif
#endif
