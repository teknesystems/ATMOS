// Template forward declarations required for multiarray related classes
#ifndef ATMOS_MULTIARRAY_FWD_INCLUDED
#define ATMOS_MULTIARRAY_FWD_INCLUDED

namespace ATMOS
{

  template<class IT, class ST> struct multiarray_iterator;
  template<class IT, class ST> struct multiarray_view;
  template<class IT, class ST> struct const_multiarray_iterator;
  template<class IT, class ST> struct const_multiarray_view;
  template<class T, class ST> struct multiarray;

}

#endif
