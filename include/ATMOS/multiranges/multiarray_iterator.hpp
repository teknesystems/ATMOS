// An iterator over a dimension of a multiarray.
// Satisfies RandomAccessIterator and boost::MultiArray

#ifndef ATMOS_MULTIARRAY_ITERATOR
#define ATMOS_MULTIARRAY_ITERATOR

#include "multiarray_fwd.hpp"

namespace ATMOS
{

  template<
    class IT, // Wrapped iterator. Must satisfy RandomAccessIterator
    class ST // Storage type
  >
  struct multiarray_iterator
  {

  };

}

#endif
