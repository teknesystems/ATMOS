// Types for representing dimension and for generating C-style arrays

// Header guard. Skips over entire header if already included.
#ifndef ATMOS_MULTIARRAY_TYPES_INCLUDED
#define ATMOS_MULTIARRAY_TYPES_INCLUDED

#include <cstddef>

namespace ATMOS
{

  // Basic types
  typedef std::size_t dim_t; // Dimensional type
  typedef std::ptrdiff_t sdim_t; // Signed dimensional/index type

  // Template for generating a C-style multiarray of type T
  template<class T, std::size_t N, std::size_t... Ns>
  struct C_multiarray
  {
    typedef typename C_multiarray<T, Ns...>::type type[N];
  };

  template<class T, std::size_t N>
  struct C_multiarray<T, N>
  {
    typedef T type[N];
  };

  template<class T, std::size_t... Ns>
  using C_multiarray_t = typename C_multiarray<T, Ns...>::type;

  // C-style multiarray doctest (mainly to see the right type compiled)
  #ifdef DOCTEST_LIBRARY_INCLUDED

  TEST_CASE("C-style multiarray declarations")
  {
    int x[1][2][3] = {{{1}}};
    C_multiarray_t<int, 1, 2, 3> y = {{{1}}};

    CHECK(x[0][0][0] == y[0][0][0]);
    CHECK(x[0][1][2] == y[0][1][2]);
  }

  #endif

}

#endif
