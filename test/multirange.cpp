// Use default doctest main function
#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include <doctest.h>

// Import doctests from header files
#include <ATMOS/multiranges/multiarray_types.hpp>
#include <ATMOS/multiranges/static_vector.hpp>
#include <ATMOS/multiranges/static_stride.hpp>
#include <ATMOS/multiranges/dynamic_stride.hpp>
#include <ATMOS/multiranges/static_storage.hpp>
#include <ATMOS/multiranges/multiarray_iterator.hpp>
#include <ATMOS/multiranges/multiarray_view.hpp>
