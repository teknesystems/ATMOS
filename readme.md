Automatically Tuned Mathematical Object System (ATMOS) is an experimental header-only library providing efficient representations of various mathematical objects.

ATMOS includes optional tests which require the doctest library to run. They are not compiled in if doctest is not included. 
